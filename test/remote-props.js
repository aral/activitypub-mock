// remote-props.js -- properties for a remote server
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

module.exports = {
  port: 8081,
  address: '127.0.0.1',
  urlRoot: 'https://remote.test',
  users: {
    'remote5': {
      type: 'Person',
      name: 'User Three'
    },
    'remote4': {
      type: 'Person',
      name: 'User Four'
    }
  },
  oauth2: {
    remote5: '7f67ac69-ddb4-4aae-a359-394b39e1def5',
    remote4: '1a69728a-76d0-40ba-8723-dac3fd18f785'
  },
  rsa: {
    remote5: {
      public: '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAoHuZiPVsK4X5LlzaNqT/hPRg8auXASIFJfNhsagMhP7jaS9gQ0ZlzGvUi/3J\ngGpcq44oSM9q5CJhbjct87c3bl93O5D3okurTk0M1Sl/1LO6rijJSyd3HLenOoYdK0QKTWEz\nOZioe2fkJljng/KkwCqsBPHgj44Fmcq9ScFcTFXDX6E4rHpkRaUgO/s0nDppCTF1mtE1BBuX\nJJRsHx4J8me7zAe4IL2Pz9vHtwEN3ShFQs0PitsSbnRO+06d1EDueiYt2yqbNZPdRNqzyFoE\nq15iBoEvDHVMkATYRoUWcsZpDgYBCoZVDxfrGSZ/Kfq8TwLb04HjzCFKF8z/SCTEYwIDAQAB\n-----END RSA PUBLIC KEY-----\n',
      private: '-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEAoHuZiPVsK4X5LlzaNqT/hPRg8auXASIFJfNhsagMhP7jaS9gQ0ZlzGvU\ni/3JgGpcq44oSM9q5CJhbjct87c3bl93O5D3okurTk0M1Sl/1LO6rijJSyd3HLenOoYdK0QK\nTWEzOZioe2fkJljng/KkwCqsBPHgj44Fmcq9ScFcTFXDX6E4rHpkRaUgO/s0nDppCTF1mtE1\nBBuXJJRsHx4J8me7zAe4IL2Pz9vHtwEN3ShFQs0PitsSbnRO+06d1EDueiYt2yqbNZPdRNqz\nyFoEq15iBoEvDHVMkATYRoUWcsZpDgYBCoZVDxfrGSZ/Kfq8TwLb04HjzCFKF8z/SCTEYwID\nAQABAoIBAQAI+YCHkToAYRijSBwH7EhB1izJtw/Vx2JEUg0Hk19H3L0496ZJfNlaijkzrvii\nzs/p5CRKlhbjNSz/reRK+dcGvB393ciLcfHdih+KmCyG55or/SnsowNdPsQCWNeuwLpWw3XK\n5ceeRxwSLOydi2c9O+WszR4CyS11AUp+omqX9ij9XKZxQWz8lOsVUKEaH6rkFXiZ00Lv4pg4\nCIcCR5IR32qFdpGbWHOZNTpaCrQJmawhoYK276cCUbYSkykeW+y2CDI1h9xi4e1Z+5vqgkzb\n+9UBAqNju5WfpUiiDIlzqRpBs5r1RiIrwFvSDBDAX++neok0AcJxzGNrEdeXKUfJAoGBAOdg\nYxGPPRGLzqKWNnfZJn56IbZeXXFyuCOPCQDSMA43kP3tav3t9/Qg/0qlQzVSM6YZ2snY7SjQ\nbPvJjvDn435LzTOvkR+GVES2cAZHML+/ENo4hTgm7bEFGW+ChM/ipB9QEp0c+8xT2ElWN9nt\nB/NUfWQn9jXQxcWhKI60FRA/AoGBALGPyZZAheFb5XZMCAsmoUqerr9a6phj8up+pswgQUo1\nz4UFKb/ctnRhno/607QPV2iEICO3feCJ7m5YkapxN29YixzVv8RGzQaThL0khmxeUcrNPapQ\nrmtUg1A5ndJ1aZxRe8LcxVERr4+2Ojy2OsxfTTDN6hXPSzouVFh62sLdAoGBALl8yQ8bsM5v\nMUGggJwF5tlAGeS0s96uJDbFhWc2GbZcD42yFrLpYn1BMshpwn5tnuBLG93zXPT6WlRkGobA\nr9zg8em8lvAZ5VaMlzRBQDCIAP+gjhQdCTJ/rwt5K/XocMB1leaEhMwib2MjjnTTlCsbkcaI\nTtaHvvUXpIb6kWRLAoGAGOd59sEGBh3FiVoQpr+8T6MCD4TI8qbSI2GwM2Eyxx3VBWs799UV\nIi2u97LOjqq4L8iYWWobFatC7ecuRyGqDMv2dwzj4Lutdgaqz3yX/gscnb07sO+wBw3LfS0X\nnbUqIuqq2SHt6vj4DsgYUvucM54KTZYUBwsUGgz1d5ODq2ECgYEAtY3Qr/jRH2O/+7T8f7Fb\n4lcxBfIEaBj8Qs3ZzttKzn+P8DFKW4ynudTDzoUZBOBO9fM+WHmcS+v1fFfU4nr0PnOWimaz\n7X5treS31MBf/Bbm9M3Dp5Zi8TxMr1Wt2BgCwm3aI3PM5jna3Sto1u0SVVTGkwrWlHYPmtEJ\nZowZZe4=\n-----END RSA PRIVATE KEY-----\n'
    },
    remote4: {
      public: '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEA6EBeDoAtqpF+f3GO4jo9BGbZ1kXZQVNDmg6fmH0/wPpYmaKaLu8YMtZelv5B\nNow58rLDVhZtzeI3vfIq5MKsQ+Sa4/6W+95Qn5MnxFbi9nU66KYvdL/Lls+I7QY1BoljvIir\nqKIzi1bs8BShgj4DIw9EF5Gh68B17F7/t7eaU8yXNKBwSRtpBAsqFzKsRCY0MGi5l8dNY25A\nc8fj8RFe/MD6Zbwj2GjAOjZOojGBpiOzCFLNXAPWYIkKHP5JjBlXoORNTky6q7FRlAHi9oLe\nA/0p0ws3WM4B05yAKA6i6FtbkJDbN15Hjy0fsg5wpNedXbYjdoE0ka1b5rxsSy00HQIDAQAB\n-----END RSA PUBLIC KEY-----\n',
      private: '-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA6EBeDoAtqpF+f3GO4jo9BGbZ1kXZQVNDmg6fmH0/wPpYmaKaLu8YMtZe\nlv5BNow58rLDVhZtzeI3vfIq5MKsQ+Sa4/6W+95Qn5MnxFbi9nU66KYvdL/Lls+I7QY1Bolj\nvIirqKIzi1bs8BShgj4DIw9EF5Gh68B17F7/t7eaU8yXNKBwSRtpBAsqFzKsRCY0MGi5l8dN\nY25Ac8fj8RFe/MD6Zbwj2GjAOjZOojGBpiOzCFLNXAPWYIkKHP5JjBlXoORNTky6q7FRlAHi\n9oLeA/0p0ws3WM4B05yAKA6i6FtbkJDbN15Hjy0fsg5wpNedXbYjdoE0ka1b5rxsSy00HQID\nAQABAoIBAG2JNhMGtwL0HcIDj8z4hKCX1XNue/mISKDW9sRXhgMAz1qeSrOKBlKrse7quVz9\no7LoRqJKb70jTQDjvwEnI1Hd9oNDJnhhsIqiP6YlCGQOhBsHjeg1MNLOzJhlTgroL0bS+orM\n/Rxhgdv1Dvs7PgWCBK5M/B4RJky1vUsrZ7jPJeHSWNep7TRMI/gWVkL7WjfsJ8mzsbvRPTYq\n2bWjjURFamoqbHdxuueK8nEvTsr500IZD2vKoDIYthc8AcLYUkMuhQLDmivA52/LYRCePhtR\n+GqWsFOQOQwsX2nm49ZvEbzsG6wa0jdsV1cNJh/O+fqkiMfjN1ESoJBEIqcCTg0CgYEA/vKv\nD/UQ0UTDKqAy72u4zbKDhKwXubZqRZ7zwQ0gQS/Ci83kWd+dBqHVi5A6JgUQFDjDY4FwbNN2\nD0LnnTHFZVER5V0Bii+0HJICwKWBeiVtiCFp7SGapquGgPrakSAtjH3t6IqWDYpGnGUCavAu\n4a86XxJpzq6O9NoogIF7LI8CgYEA6TW1OWJr50wtncfVGvrzlNzO69aP0ZDY76FWK66moCAc\nM5ON0nRAVGYdC59qLTy9IkLSy2vjxr0HpeZnVdpZEorrIJiGwbzbJfpP5Yx0K2l7EZNsX984\nfHCvjjPif5yjpiyrV2MKQEdSC4J7FmdATWnEw7d0G/buQHZhIDsOgpMCgYAeHRI6g0raPRIE\nbem2R/YLLePE/wLKZ9aDpLPMAL3nlTmYg9hOGVCV4PW1R3Lm3MA6cz34+egKoquWP+PVuHRF\ndF71xZeR9/KBUWnL0YDKqbMXWkMFxGtc9nOrtnWsoK1go9s/FWeF7+hJPtEbSoVvGOB/LFCd\nHhc6qq4QB7tB1wKBgQDlMFIs1iwDQRtnf5dQj6ZI+wQkRFZM91QrlFwSS+8XWNc+XNm5JWR/\nTWdoH8ACOzoI7LErF5nfm7Bje168wfxdu8a93wg4aDjbRve0OWeFroEzUbXlNoPV0AzH7PfA\nwBkYW7f8BqpxZ2YbR51d9BT+v16PlcB3JyMYmP505XUqHwKBgQCZmbCMBjCTxHgexb7tL1BQ\nlNl4uLeUvzKTDv3tmW0UbTjUP8X8Iy78cq/cZJZzFiljDXivWPfILOrQBHz3+bNTukUwSGNJ\n3PWh6WS+FSbnZCzyMgpGkNYmt3CwdQvPcIH+EOiX/0Le6H8VU4+0j0Wy9/hwJguFURb95yVT\nh1Fksw==\n-----END RSA PRIVATE KEY-----\n'
    }
  },
  rootMap: {
    'https://remote.test': 'http://localhost:8081',
    'https://activitypub.test': 'http://localhost:8080'
  }
}
