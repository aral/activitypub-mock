// user-test.js -- GET /user/:id
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:start-stop-test')

const props = require('./props')
const mockBatch = require('./mock-batch')
const {AS2_TYPE, JSONLD_TYPE, JSON_TYPE} = require('./media-types')
const {bt, mtb} = require('./batch-utils')

const USER1 = `http://${props.address}:${props.port}/user/user1`

const SEC = 'https://w3id.org/security/v1'

vows.describe('GET /user/:id')
  .addBatch(mockBatch({
    'and we get a user': {
      async topic () {
        const url = USER1
        return fetch(url, {headers: {Accept: JSONLD_TYPE}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
        debug(res.headers)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has the correct ID': bt('id', 'string', `${props.urlRoot}/user/user1`),
        'it has the correct type': bt('type', 'string', props.users.user1.type),
        'it has the correct name': bt('name', 'string', props.users.user1.name),
        'it has a published date': bt('published', 'string'),
        'it has an inbox': bt('ldp:inbox', 'string', `${props.urlRoot}/user/user1/inbox`),
        'it has an outbox': bt('as:outbox', 'string', `${props.urlRoot}/user/user1/outbox`),
        'it has followers': bt('as:followers', 'string', `${props.urlRoot}/user/user1/followers`),
        'it has following': bt('as:following', 'string', `${props.urlRoot}/user/user1/following`),
        'it has liked': bt('as:liked', 'string', `${props.urlRoot}/user/user1/liked`),
        'and we get the public key': {
          topic (body) {
            return body[`${SEC}publicKey`]
          },
          'it works': (err, key) => {
            assert.ifError(err)
            assert.isObject(key)
          },
          'it has an ID': bt(`id`, 'string', `${props.urlRoot}/user/user1/publickey`),
          'it has a type': bt(`type`, 'string', `${SEC}Key`),
          'it has a summary': bt(`summary`, 'string', 'public key for user1'),
          'it has an owner': bt(`${SEC}owner`, 'string', `${props.urlRoot}/user/user1`),
          'it has a publicKeyPem': bt(`${SEC}publicKeyPem`, 'string')
        }
      }
    },
    'and we get a user with the AS2 media type': mtb(USER1, AS2_TYPE),
    'and we get a user with the JSON media type': mtb(USER1, JSON_TYPE)
  }))
  .export(module)
