// distribute-local-test.js -- distribute activities to local addressees
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:distribute-local')

const props = require('./props')
const mockBatch = require('./mock-batch')

const {JSONLD_TYPE} = require('./media-types')

const OUTBOX1 = `http://${props.address}:${props.port}/user/user1/outbox`
const INBOX2 = `http://${props.address}:${props.port}/user/user2/inbox`

vows.describe('Distribute activities to local addressees')
  .addBatch(mockBatch({
    'and we post to an outbox': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            Authorization: `Bearer ${props.oauth2.user1}`,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Listen',
            summary: 'User1 listened to The Promise',
            to: 'https://www.w3.org/ns/activitystreams#Public',
            object: {
              type: 'Audio',
              id: 'https://musicbrainz.org/recording/d9f96478-a818-4382-a777-b455877f30dc',
              name: 'The Promise'
            }
          })
        })
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'and we get the inbox of a follower': {
          topic () {
            return fetch(INBOX2, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user2}`}})
          },
          'it works': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
          },
          'and we get the inbox body': {
            topic (res) {
              return res.json()
            },
            'it works': (err, body) => {
              assert.ifError(err)
              assert.isObject(body)
              debug(body)
            },
            'it has the right size': (err, body) => {
              assert.ifError(err)
              assert.isObject(body)
              assert.include(body, 'first')
              assert.isObject(body.first)
              assert.include(body.first, 'items')
              assert.isObject(body.first.items)
            },
            'and we look up the posted activity in the inbox body': {
              topic (body, res, activity) {
                return [body.first.items.id, activity.id]
              },
              'it works': (err, ids) => {
                assert.ifError(err)
                assert.isArray(ids)
                assert.equal(ids[0], ids[1])
              }
            }
          }
        }
      }
    }
  }))
  .export(module)
