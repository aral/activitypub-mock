// distribute-remote-test.js -- distribute activities to remote addressees
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:distribute-remote')

const props = require('./props')
const remoteProps = require('./remote-props')
const mockBatch = require('./mock-batch')

const {JSONLD_TYPE} = require('./media-types')

const OUTBOX1 = `http://${props.address}:${props.port}/user/user0/outbox`
const INBOX2 = `http://${remoteProps.address}:${remoteProps.port}/user/remote5/inbox`

vows.describe('Distribute activities to remote addressees')
  .addBatch(mockBatch({
    'and we post to an outbox': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            Authorization: `Bearer ${props.oauth2.user0}`,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Read',
            summary: 'user0 read the ActivityPub specification',
            to: 'https://www.w3.org/ns/activitystreams#Public',
            object: {
              type: 'Document',
              id: 'https://www.w3.org/TR/activitypub/',
              name: 'ActivityPub'
            }
          })
        })
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'and we wait a couple of seconds for distribution': {
          async topic () {
            return new Promise((resolve, reject) => {
              setTimeout(resolve, 2000)
            })
          },
          'it works': (err) => {
            assert.ifError(err)
          },
          'and we get the inbox of a remote follower': {
            topic () {
              return fetch(INBOX2, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${remoteProps.oauth2.remote5}`}})
            },
            'it works': (err, res) => {
              assert.ifError(err)
              assert.isObject(res)
            },
            'and we get the inbox body': {
              topic (res) {
                return res.json()
              },
              'it works': (err, body) => {
                assert.ifError(err)
                assert.isObject(body)
                debug(body)
              },
              'it has the right size': (err, body) => {
                assert.ifError(err)
                assert.isObject(body)
                assert.include(body, 'first')
                assert.isObject(body.first)
                assert.include(body.first, 'items')
                assert.isObject(body.first.items)
              },
              'and we look up the posted activity in the inbox body': {
                topic (body, res, ignore, activity) {
                  // Bug in perjury when there's no result for a promise
                  if (!activity) {
                    activity = ignore
                  }
                  debug(body)
                  debug(activity)
                  return [body.first.items.id, activity.id]
                },
                'it works': (err, ids) => {
                  assert.ifError(err)
                  assert.isArray(ids)
                  assert.equal(ids[0], ids[1])
                }
              }
            }
          }
        }
      }
    }
  }))
  .export(module)
